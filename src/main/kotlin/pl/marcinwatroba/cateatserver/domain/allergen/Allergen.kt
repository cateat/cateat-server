package pl.marcinwatroba.cateatserver.domain.allergen

import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementEntity
import javax.persistence.*

/**
 * Created by Marcin
 */
@Entity
data class Allergen(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long,
        var name: String,
        var description: String,

        @ManyToMany(mappedBy = "allergens")
        var menuElements: Set<MenuElementEntity>
) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Allergen

                if (id != other.id) return false
                if (name != other.name) return false
                if (description != other.description) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id.hashCode()
                result = 31 * result + name.hashCode()
                result = 31 * result + description.hashCode()
                return result
        }

        override fun toString(): String {
                return "Allergen(id=$id, name='$name', description='$description')"
        }
}