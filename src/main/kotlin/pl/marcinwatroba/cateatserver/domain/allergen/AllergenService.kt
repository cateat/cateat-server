package pl.marcinwatroba.cateatserver.domain.allergen

import org.springframework.stereotype.Service

/**
 * Created by Marcin
 */
@Service
class AllergenService(
        private val allergenRepository: AllergenRepository
) {
    fun convertSetToEntities(nameSet: Set<String>) =
            nameSet.map { getAllergenByName(it) }.toSet()

    private fun getAllergenByName(name: String) =
            allergenRepository.getByName(name) ?: throw Exception("ALLERGEN_NOT_EXISTS")
}