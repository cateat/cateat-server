package pl.marcinwatroba.cateatserver.domain.allergen

import org.springframework.data.repository.CrudRepository

/**
 * Created by Marcin
 */
interface AllergenRepository : CrudRepository<Allergen, Long> {

    fun getByName(name: String): Allergen?

}