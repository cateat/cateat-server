package pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model

import org.springframework.data.repository.CrudRepository
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity

/**
 * Created by Marcin
 */
interface FirebaseMessageDeviceRepository : CrudRepository<FirebaseMessageDeviceEntity, String> {

    fun findByTokenAndEatingPlace(token: String, eatingPlace: EatingPlaceEntity): FirebaseMessageDeviceEntity?

}