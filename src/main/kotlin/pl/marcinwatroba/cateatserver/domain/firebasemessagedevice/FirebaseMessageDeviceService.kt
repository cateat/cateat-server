package pl.marcinwatroba.cateatserver.domain.firebasemessagedevice

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceRepository
import pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model.CreateFirebaseMessageDevice
import pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model.FirebaseMessageDeviceEntity
import pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model.FirebaseMessageDeviceRepository
import pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model.FirebaseMessageDeviceLevel
import pl.marcinwatroba.cateatserver.global.extension.getOrThrow

/**
 * Created by Marcin
 */
@Service
class FirebaseMessageDeviceService(
        private val eatingPlaceRepository: EatingPlaceRepository,
        private val firebaseMessageDeviceRepository: FirebaseMessageDeviceRepository
) {

    fun updateToken(placeId: String, body: CreateFirebaseMessageDevice) {
        val place = eatingPlaceRepository.findById(placeId).getOrThrow("PLACE_NOT_FOUND")
        val value = firebaseMessageDeviceRepository
                .findByTokenAndEatingPlace(body.token, place)
        if (value == null) {
            val newValue = FirebaseMessageDeviceEntity(token = body.token, eatingPlace = place,
                    level = FirebaseMessageDeviceLevel.valueOf(body.level))
            place.firebaseMessageDevices = place.firebaseMessageDevices.plus(newValue)
            firebaseMessageDeviceRepository.save(newValue)
            eatingPlaceRepository.save(place)
        } else {
            value.apply {
                level = FirebaseMessageDeviceLevel.valueOf(body.level)
            }
            firebaseMessageDeviceRepository.save(value)
        }
    }

}