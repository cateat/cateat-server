package pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model

import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

/**
 * Created by Marcin
 */
@Entity
data class FirebaseMessageDeviceEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var token: String,
        var created: LocalDate = LocalDate.now(),
        var expired: LocalDate? = null,
        var level: FirebaseMessageDeviceLevel,

        @ManyToOne
        var eatingPlace: EatingPlaceEntity
) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as FirebaseMessageDeviceEntity

                if (id != other.id) return false
                if (token != other.token) return false
                if (created != other.created) return false
                if (expired != other.expired) return false
                if (level != other.level) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id.hashCode()
                result = 31 * result + token.hashCode()
                result = 31 * result + created.hashCode()
                result = 31 * result + (expired?.hashCode() ?: 0)
                result = 31 * result + level.hashCode()
                return result
        }

        override fun toString(): String {
                return "FirebaseMessageDeviceEntity(id='$id', token='$token', created=$created, expired=$expired, " +
                        "level=$level)"
        }
}