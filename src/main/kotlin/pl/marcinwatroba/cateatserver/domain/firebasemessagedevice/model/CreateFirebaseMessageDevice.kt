package pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model

/**
 * Created by Marcin
 */
data class CreateFirebaseMessageDevice(
        val token: String,
        var level: String
)