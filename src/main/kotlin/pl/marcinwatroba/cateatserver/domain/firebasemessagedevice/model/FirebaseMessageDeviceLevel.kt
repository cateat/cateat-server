package pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model

/**
 * Created by Marcin
 */
enum class FirebaseMessageDeviceLevel {
    NOTHING,
    ONLY_SPECIAL,
    ALL
}