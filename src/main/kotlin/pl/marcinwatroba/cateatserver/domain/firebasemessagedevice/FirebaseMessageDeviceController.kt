package pl.marcinwatroba.cateatserver.domain.firebasemessagedevice

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model.CreateFirebaseMessageDevice

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("firebaseNotificationDeviceController")
class FirebaseMessageDeviceController(
        private val firebaseMessageDeviceService: FirebaseMessageDeviceService
) {

    @PostMapping("/{placeId}")
    @ResponseStatus(HttpStatus.CREATED)
    fun updateToken(@PathVariable placeId: String, @RequestBody body: CreateFirebaseMessageDevice) {
        firebaseMessageDeviceService.updateToken(placeId, body)
    }

}