package pl.marcinwatroba.cateatserver.domain.mealcategory.model

import org.springframework.data.repository.CrudRepository
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity

/**
 * Created by Marcin
 */
interface MealCategoryRepository : CrudRepository<MealCategoryEntity, String> {
    fun findAllByDeletedIsFalseAndEatingPlace(eatingPlace: EatingPlaceEntity): List<MealCategoryEntity>
}