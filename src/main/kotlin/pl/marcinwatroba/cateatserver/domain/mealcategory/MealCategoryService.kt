package pl.marcinwatroba.cateatserver.domain.mealcategory

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceRepository
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceService
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.CreateMealCategory
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryDto
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryEntity
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryRepository
import pl.marcinwatroba.cateatserver.global.extension.getOrThrow
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@Service
class MealCategoryService(
        private val eatingPlaceService: EatingPlaceService,
        private val mealCategoryRepository: MealCategoryRepository,
        private val eatingPlaceRepository: EatingPlaceRepository
) {

    fun getAuthenticatedCategory(username: String, placeId: String, categoryId: String): MealCategoryEntity {
        eatingPlaceService.getAuthenticatedPlace(username, placeId)
        val category = mealCategoryRepository.findById(categoryId)
                .getOrThrow("CATEGORY_NOT_FOUND")
        if (category.eatingPlace.id != placeId) {
            throw Exception("CATEGORY_NOT_FOUND")
        }
        return category
    }

    fun addCategory(loggedUser: OAuth2UserDto, placeId: String, body: CreateMealCategory) {
        val place = eatingPlaceService.getAuthenticatedPlace(loggedUser.username, placeId)
        val newCategory = MealCategoryEntity(name = body.name, description = body.description, eatingPlace = place,
                active = body.active)
        place.mealCategories = place.mealCategories.plus(newCategory)
        mealCategoryRepository.save(newCategory)
        eatingPlaceRepository.save(place)
    }

    fun removeCategory(loggedUser: OAuth2UserDto, placeId: String, categoryId: String) {
        val category = getAuthenticatedCategory(loggedUser.username, placeId, categoryId)
        if (category.deleted) {
            throw Exception("REMOVED_ALREADY")
        }
        category.deleted = true
        mealCategoryRepository.save(category)
    }

    fun editCategory(loggedUser: OAuth2UserDto, placeId: String, body: CreateMealCategory) {
        val category = getAuthenticatedCategory(loggedUser.username, placeId, body.id!!)
        if (category.deleted) {
            throw Exception("REMOVED_ALREADY")
        }
        category.apply {
            name = body.name
            description = body.description
            active = body.active
        }
        mealCategoryRepository.save(category)
    }

    fun getCategories(loggedUser: OAuth2UserDto, placeId: String): Set<MealCategoryDto> {
        val place = eatingPlaceService.getAuthenticatedPlace(loggedUser.username, placeId)
        return mealCategoryRepository.findAllByDeletedIsFalseAndEatingPlace(place)
                .map { MealCategoryDto.create(it) }.toSet()
    }

}