package pl.marcinwatroba.cateatserver.domain.mealcategory.model

/**
 * Created by Marcin
 */
data class CreateMealCategory(
        var id: String?,
        var name: String,
        var description: String?,
        var active: Boolean
)