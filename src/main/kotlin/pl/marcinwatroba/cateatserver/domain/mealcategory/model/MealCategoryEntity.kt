package pl.marcinwatroba.cateatserver.domain.mealcategory.model

import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import pl.marcinwatroba.cateatserver.domain.servedmeal.model.ServedMealEntity
import java.util.*
import javax.persistence.*

/**
 * Created by Marcin
 */
@Entity
data class MealCategoryEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var name: String,
        var description: String?,
        var deleted: Boolean = false,
        var active: Boolean,

        @ManyToOne
        var eatingPlace: EatingPlaceEntity,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "mealCategory")
        var servedMeals: Set<ServedMealEntity> = emptySet()

) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as MealCategoryEntity

                if (id != other.id) return false
                if (name != other.name) return false
                if (description != other.description) return false
                if (deleted != other.deleted) return false
                if (active != other.active) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id.hashCode()
                result = 31 * result + name.hashCode()
                result = 31 * result + (description?.hashCode() ?: 0)
                result = 31 * result + deleted.hashCode()
                result = 31 * result + active.hashCode()
                return result
        }

        override fun toString(): String {
                return "MealCategoryEntity(id='$id', name='$name', description=$description, deleted=$deleted, active=$active)"
        }
}