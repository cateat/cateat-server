package pl.marcinwatroba.cateatserver.domain.mealcategory

import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.CreateMealCategory
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("/category")
class MealCategoryController(
        private val menuCategoryService: MealCategoryService
) {

    @PostMapping("/{placeId}")
    @ResponseStatus(HttpStatus.CREATED)
    fun addCategory(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                    @RequestBody body: CreateMealCategory) {
        menuCategoryService.addCategory(loggedUser, placeId, body)
    }

    @DeleteMapping("/{placeId}/{categoryId}")
    @ResponseStatus(HttpStatus.OK)
    fun removeCategory(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                       @PathVariable categoryId: String) {
        menuCategoryService.removeCategory(loggedUser, placeId, categoryId)
    }

    @PutMapping("/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun editCategory(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                     @RequestBody body: CreateMealCategory) {
        menuCategoryService.editCategory(loggedUser, placeId, body)
    }

    @GetMapping("/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun getCategories(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String) =
            menuCategoryService.getCategories(loggedUser, placeId)

}