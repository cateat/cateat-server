package pl.marcinwatroba.cateatserver.domain.mealcategory.model

import java.util.*

/**
 * Created by Marcin
 */
data class MealCategoryDto(
        var id: String = UUID.randomUUID().toString(),
        var name: String,
        var description: String?,
        var deleted: Boolean,
        var active: Boolean
) {
    companion object {
        fun create(mealCategoryEntity: MealCategoryEntity) = with(mealCategoryEntity) {
            MealCategoryDto(id, name, description, deleted, active)
        }
    }

}