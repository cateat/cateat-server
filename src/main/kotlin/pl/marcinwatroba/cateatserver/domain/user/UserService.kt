package pl.marcinwatroba.cateatserver.domain.user

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.user.dto.CreateAccountDto
import pl.marcinwatroba.cateatserver.domain.user.dto.UserDto
import pl.marcinwatroba.cateatserver.notification.email.EmailService
import pl.marcinwatroba.cateatserver.security.domain.OAuth2Facade
import pl.marcinwatroba.cateatserver.security.dto.CreateOAuth2UserDto
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@Service
class UserService(
        private val oAuth2Facade: OAuth2Facade,
        private val userRepository: UserRepository,
        private val emailService: EmailService
) {

    fun createAccount(model: CreateAccountDto) {

        if (oAuth2Facade.findUserByEmail(model.email) != null) {
            throw Exception("EMAIL_EXISTS")
        }
        oAuth2Facade.createUserCredentials(CreateOAuth2UserDto(model.email, model.password))
        val activationCode = oAuth2Facade.getUserActivationKeys(model.email)
        userRepository.save(UserEntity(email = model.email, eatingPlaces = emptySet(), firstName = model.firstName,
                lastName = model.lastName))
        println(activationCode)

        // TODO(make better email)
        emailService.sendSimpleMessage(model.email, "Rejestracja", activationCode.toString())
    }

    fun activateAccount(userId: String, userActivationLink: String) {
        val status = oAuth2Facade.activateAccount(userId, userActivationLink)
        when (status) {
            OAuth2Facade.ActivationStatus.BAD_CREDENTIALS -> {
                throw Exception("BAD_CREDENTIALS")
            }
            OAuth2Facade.ActivationStatus.JUST_ACTIVATED -> {
                throw Exception("JUST_ACTIVATED")
            }
            else -> {
            }
        }
    }

    fun getUsersFromPlace(loggedUser: OAuth2UserDto, placeId: String): Set<UserDto> {
        val user = getUser(loggedUser.username)
        val place = user.eatingPlaces.find { it.id == placeId } ?: throw Exception("PLACE_NOT_FOUND")
        return place.users.map { UserDto.create(it) }.toSet()
    }

    fun getUser(email: String) = userRepository.findUserByEmail(email)
            ?: throw Exception("USER_NOT_FOUND")

    fun getUser(loggedUser: OAuth2UserDto) = UserDto.create(getUser(loggedUser.username))

}