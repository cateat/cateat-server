package pl.marcinwatroba.cateatserver.domain.user

import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.user.dto.CreateAccountDto
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("/user")
class UserController(
        val userService: UserService
) {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/createAccount")
    fun createAccount(@RequestBody body: CreateAccountDto) {
        userService.createAccount(body)
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/activate/{userId}/{token}")
    fun confirmAccount(@PathVariable userId: String, @PathVariable token: String) {
        userService.activateAccount(userId, token)
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/fromPlace/{placeId}")
    fun getFromPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String) =
            userService.getUsersFromPlace(loggedUser, placeId)

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("")
    fun getUser(@AuthenticationPrincipal loggedUser: OAuth2UserDto) =
        userService.getUser(loggedUser)

    @GetMapping("/changeEmail")
    fun changeEmail() {
        TODO()
    }

    @GetMapping("/changePassword")
    fun changePassword() {
        TODO()
    }

    @GetMapping("/resetPassword")
    fun resetPassword() {
        TODO()
    }

}
