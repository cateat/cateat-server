package pl.marcinwatroba.cateatserver.domain.user.dto

import pl.marcinwatroba.cateatserver.domain.user.UserEntity

/**
 * Created by Marcin
 */
data class UserDto(
        val id: String,
        val email: String,
        val firstName: String,
        val lastName: String
) {

    companion object {
        fun create(user: UserEntity) = with(user) {
            UserDto(id, email, firstName, lastName)
        }
    }

}