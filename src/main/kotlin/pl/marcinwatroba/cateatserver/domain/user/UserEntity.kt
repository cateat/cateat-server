package pl.marcinwatroba.cateatserver.domain.user

import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import pl.marcinwatroba.cateatserver.domain.firebasemessage.model.FirebaseMessageEntity
import java.util.*
import javax.persistence.*

/**
 * Created by Marcin
 */
@Entity
data class UserEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var email: String,
        var firstName: String,
        var lastName: String,

        @ManyToMany(fetch = FetchType.LAZY,
                cascade = [
                    CascadeType.PERSIST,
                    CascadeType.MERGE
                ])
        @JoinTable(name = "userWithEatingPlaces",
                joinColumns = [JoinColumn(name = "userId")],
                inverseJoinColumns = [JoinColumn(name = "placeId")])
        var eatingPlaces: Set<EatingPlaceEntity>,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "eatingPlace")
        var firebaseMessages: Set<FirebaseMessageEntity> = emptySet()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserEntity

        if (id != other.id) return false
        if (email != other.email) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + email.hashCode()
        return result
    }

    override fun toString(): String {
        return "UserEntity(id='$id', email='$email')"
    }

}