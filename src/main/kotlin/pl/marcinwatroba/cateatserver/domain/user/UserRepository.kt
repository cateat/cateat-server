package pl.marcinwatroba.cateatserver.domain.user

import org.springframework.data.repository.CrudRepository

/**
 * Created by Marcin
 */
interface UserRepository : CrudRepository<UserEntity, String> {

    fun findUserByEmail(id: String): UserEntity?

}