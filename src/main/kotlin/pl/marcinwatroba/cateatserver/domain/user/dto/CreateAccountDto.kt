package pl.marcinwatroba.cateatserver.domain.user.dto

/**
 * Created by Marcin
 */
data class CreateAccountDto(
        val email: String,
        val password: String,
        val firstName: String,
        val lastName: String
)
