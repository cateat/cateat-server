package pl.marcinwatroba.cateatserver.domain.servedmeal.model

/**
 * Created by Marcin
 */
data class AddElementToServedMealBody(
        val elementId: String
)