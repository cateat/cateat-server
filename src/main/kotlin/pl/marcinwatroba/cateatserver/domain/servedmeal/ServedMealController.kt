package pl.marcinwatroba.cateatserver.domain.servedmeal

import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.servedmeal.model.AddElementToServedMealBody
import pl.marcinwatroba.cateatserver.domain.servedmeal.model.CreateServedMeal
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("/servedMeal")
class ServedMealController(
        private val servedMealService: ServedMealService
) {

    @PostMapping("/{placeId}")
    fun addServedMeal(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                      @RequestBody body: CreateServedMeal) {
        servedMealService.addServedMeal(loggedUser, placeId, body)
    }

    @DeleteMapping("/{placeId}/{categoryId}/{servedMealId}")
    fun removeServedMeal(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                         @PathVariable categoryId: String, @PathVariable servedMealId: String) {
        servedMealService.removeServedMeal(loggedUser, placeId, categoryId, servedMealId)
    }

    @PutMapping("/{placeId}/{categoryId}")
    fun updateServedMeal(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                         @PathVariable categoryId: String, @RequestBody body: CreateServedMeal) {
        servedMealService.updateServedMeal(loggedUser, placeId, categoryId, body)
    }

    @GetMapping("/{placeId}")
    fun getServedMeals(@PathVariable placeId: String) = servedMealService.getServedMeals(placeId)

}