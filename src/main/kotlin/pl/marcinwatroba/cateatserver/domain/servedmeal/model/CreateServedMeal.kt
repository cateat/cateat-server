package pl.marcinwatroba.cateatserver.domain.servedmeal.model

import java.time.LocalDate

/**
 * Created by Marcin
 */
data class CreateServedMeal(
        val id: String?,
        val date: LocalDate?,
        val ends: LocalDate?,
        val mealDefinitionId: String,
        val menuElements: List<String>
)