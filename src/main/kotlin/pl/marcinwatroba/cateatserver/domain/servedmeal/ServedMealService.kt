package pl.marcinwatroba.cateatserver.domain.servedmeal

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceService
import pl.marcinwatroba.cateatserver.domain.mealcategory.MealCategoryService
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryEntity
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryRepository
import pl.marcinwatroba.cateatserver.domain.menuelement.MenuElementService
import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementEntity
import pl.marcinwatroba.cateatserver.domain.servedmeal.model.*
import pl.marcinwatroba.cateatserver.global.extension.getOrThrow
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@Service
class ServedMealService(
        private val servedMealRepository: ServedMealRepository,
        private val mealCategoryService: MealCategoryService,
        private val mealCategoryRepository: MealCategoryRepository,
        private val menuElementService: MenuElementService,
        private val eatingPlaceService: EatingPlaceService
) {

    fun getAuthenticatedServedMeal(username: String, placeId: String, categoryId: String,
                                   servedMealId: String): ServedMealEntity {
        mealCategoryService.getAuthenticatedCategory(username, placeId, categoryId)
        val servedMeal = servedMealRepository.findById(servedMealId)
                .getOrThrow("SERVED_MEAL_NOT_FOUND")
        if (servedMeal.mealCategory.id != categoryId || servedMeal.deleted) {
            throw Exception("SERVED_MEAL_NOT_FOUND")
        }
        return servedMeal
    }

    fun addServedMeal(loggedUser: OAuth2UserDto, placeId: String, body: CreateServedMeal) {
        val category = mealCategoryService
                .getAuthenticatedCategory(loggedUser.username, placeId, body.mealDefinitionId)
        val servedMeal = ServedMealEntity(date = body.date, ends = body.ends, mealCategory = category)
        category.servedMeals = category.servedMeals.plus(servedMeal)
        servedMealRepository.save(servedMeal)
        mealCategoryRepository.save(category)

        val menuElements = body.menuElements
                .map { menuElementService.getAuthenticatedElement(loggedUser.username, placeId, it) }
        menuElements.forEach { addElementToServedMeal(servedMeal, it) }
        servedMealRepository.save(servedMeal)
    }

    fun addElementToServedMeal(meal: ServedMealEntity, element: MenuElementEntity) {
        meal.apply { menuElements = menuElements.plus(element) }
        servedMealRepository.save(meal)
    }

    fun removeServedMeal(loggedUser: OAuth2UserDto, placeId: String, categoryId: String, servedMealId: String) {
        val servedMeal = getAuthenticatedServedMeal(loggedUser.username, placeId, categoryId,
                servedMealId)
        servedMeal.deleted = true
        servedMealRepository.save(servedMeal)
    }

    private fun changeCategory(servedMeal: ServedMealEntity, newCategory: MealCategoryEntity) {
        val oldCategory = servedMeal.mealCategory
        oldCategory.apply { servedMeals = servedMeals.minus(servedMeal) }
        servedMeal.apply { mealCategory = newCategory }
        newCategory.apply { servedMeals = servedMeals.plus(servedMeal) }
        mealCategoryRepository.saveAll(setOf(oldCategory, newCategory))
        servedMealRepository.save(servedMeal)
    }

    fun updateServedMeal(loggedUser: OAuth2UserDto, placeId: String, categoryId: String, body: CreateServedMeal) {
        val servedMeal = getAuthenticatedServedMeal(loggedUser.username, placeId, categoryId,
                body.id!!)

        if (servedMeal.mealCategory.id != body.mealDefinitionId) {
            val newCategory = mealCategoryService.getAuthenticatedCategory(loggedUser.username,
                    placeId, body.mealDefinitionId)
            changeCategory(servedMeal, newCategory)
        }

        servedMeal.apply {
            date = body.date
            ends = body.ends
            menuElements = emptySet()
        }

        val menuElements = body.menuElements
                .map { menuElementService.getAuthenticatedElement(loggedUser.username, placeId, it) }
        menuElements.forEach { addElementToServedMeal(servedMeal, it) }
        servedMealRepository.save(servedMeal)
    }

    // TODO: this can be faster
    fun getServedMeals(placeId: String) = eatingPlaceService.getPlace(placeId).mealCategories
            .map { it.servedMeals }
            .flatMap { it.toList() }
            .filter { !it.deleted }
            .map { ServedMealDto.create(it) }
            .toSet()

}