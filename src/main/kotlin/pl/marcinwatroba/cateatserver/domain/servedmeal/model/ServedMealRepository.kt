package pl.marcinwatroba.cateatserver.domain.servedmeal.model

import org.springframework.data.repository.CrudRepository

/**
 * Created by Marcin
 */
interface ServedMealRepository : CrudRepository<ServedMealEntity, String> {

}