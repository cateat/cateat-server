package pl.marcinwatroba.cateatserver.domain.servedmeal.model

import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryEntity
import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementEntity
import java.time.LocalDate
import java.util.*
import javax.persistence.*

/**
 * Created by Marcin
 */
@Entity
data class ServedMealEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var date: LocalDate?,
        var ends: LocalDate?,
        var deleted: Boolean = false,

        @ManyToMany(fetch = FetchType.LAZY,
                cascade = [
                    CascadeType.PERSIST,
                    CascadeType.MERGE
                ])
        @JoinTable(name = "servedMealElement",
                joinColumns = [JoinColumn(name = "servedMealId")],
                inverseJoinColumns = [JoinColumn(name = "menuElementId")])
        var menuElements: Set<MenuElementEntity> = emptySet(),

        @ManyToOne
        var mealCategory: MealCategoryEntity
) {
    fun isAllTime() = date == null && ends == null

    fun isOneDay() = date != null && ends == null

    fun isInPeriod() = date != null && ends != null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ServedMealEntity

        if (id != other.id) return false
        if (date != other.date) return false
        if (ends != other.ends) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (date?.hashCode() ?: 0)
        result = 31 * result + (ends?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "ServedMealEntity(id='$id', date=$date, ends=$ends)"
    }
}