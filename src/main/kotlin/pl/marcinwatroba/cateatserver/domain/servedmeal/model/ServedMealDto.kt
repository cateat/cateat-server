package pl.marcinwatroba.cateatserver.domain.servedmeal.model

import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryDto
import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementDto
import java.time.LocalDate
import java.util.*

/**
 * Created by Marcin
 */
data class ServedMealDto(
        val id: String = UUID.randomUUID().toString(),
        val date: LocalDate?,
        val ends: LocalDate?,
        val mealCategory: MealCategoryDto,
        val menuElements: Set<MenuElementDto>
) {
    companion object {
        fun create(servedMealEntity: ServedMealEntity) = with(servedMealEntity) {
            ServedMealDto(id, date, ends, MealCategoryDto.create(mealCategory),
                    menuElements.filter { !it.deleted }.map { MenuElementDto.create(it) }.toSet())
        }
    }
}