package pl.marcinwatroba.cateatserver.domain.eatingplace

import org.springframework.data.repository.CrudRepository
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import pl.marcinwatroba.cateatserver.domain.user.UserEntity

/**
 * Created by Marcin
 */
interface EatingPlaceRepository : CrudRepository<EatingPlaceEntity, String> {
    fun findAllByDeletedIsFalseAndUsers(users: UserEntity): List<EatingPlaceEntity>
}