package pl.marcinwatroba.cateatserver.domain.eatingplace

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.eatingplace.dto.CreateJoinToPlace
import pl.marcinwatroba.cateatserver.domain.eatingplace.dto.EatingPlaceDto
import pl.marcinwatroba.cateatserver.domain.eatingplace.dto.JoinToPlaceCodeDto
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.Address
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.AllergenStrategy
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import pl.marcinwatroba.cateatserver.domain.user.UserRepository
import pl.marcinwatroba.cateatserver.domain.user.UserService
import pl.marcinwatroba.cateatserver.global.util.CodeGenerator
import pl.marcinwatroba.cateatserver.global.extension.getOrThrow
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@Service
class EatingPlaceService(
        private val userRepository: UserRepository,
        private val eatingPlaceRepository: EatingPlaceRepository,
        private val userService: UserService
) {

    fun getAuthenticatedPlace(email: String, placeId: String): EatingPlaceEntity {
        getPlace(placeId)
        return userService.getUser(email).eatingPlaces
                .find { it.id == placeId } ?: throw Exception("PLACE_NOT_FOUND")
    }

    fun getPlace(placeId: String) = eatingPlaceRepository.findById(placeId)
            .getOrThrow("PLACE_NOT_FOUND")
            .run {
                if (deleted) {
                    throw Exception("PLACE_NOT_FOUND")
                } else {
                    this
                }
            }

    fun getUserEatingPlaces(loggedUser: OAuth2UserDto): Set<EatingPlaceDto> {
        val user = userService.getUser(loggedUser.username)
        return eatingPlaceRepository.findAllByDeletedIsFalseAndUsers(user).map { EatingPlaceDto.create(it) }.toSet()
    }

    fun getJoinToPlaceCode(loggedUser: OAuth2UserDto, placeId: String) =
            JoinToPlaceCodeDto.create(getAuthenticatedPlace(loggedUser.username, placeId))

    fun changeJoinToPlaceCode(loggedUser: OAuth2UserDto, placeId: String) {
        eatingPlaceRepository.save(getAuthenticatedPlace(loggedUser.username, placeId)
                .apply { joinToPlaceCode = getCode() })
    }

    private fun getCode() = CodeGenerator.getCode(4, 4)

    fun getUserPlaceById(loggedUser: OAuth2UserDto, placeId: String): EatingPlaceDto =
            EatingPlaceDto.create(getAuthenticatedPlace(loggedUser.username, placeId))

    fun addEatingPlace(loggedUser: OAuth2UserDto, eatingPlace: EatingPlaceDto) {

        val user = userService.getUser(loggedUser.username)

        val address = with(eatingPlace.address) {
            Address(street = street, number = number, city = city, postalCode = postalCode,
                    latitude = latitude, longitude = longitude)
        }

        val place = EatingPlaceEntity(name = eatingPlace.name, description = eatingPlace.description,
                allergenStrategy = AllergenStrategy.valueOf(eatingPlace.allergenStrategy),
                address = address, phoneNumber = eatingPlace.phoneNumber, email = eatingPlace.email,
                website = eatingPlace.website, users = setOf(user), joinToPlaceCode = getCode(),
                facebookPage = eatingPlace.facebookPage, twitterPage = eatingPlace.twitterPage,
                pizzaPortalPage = eatingPlace.pizzaPortalPage, pysznePlPage = eatingPlace.pysznePlPage)

        eatingPlaceRepository.save(place)
        user.eatingPlaces = user.eatingPlaces.plus(place)
        userRepository.save(user)
    }

    fun editEatingPlace(loggedUser: OAuth2UserDto, eatingPlace: EatingPlaceDto) {

        val place = getAuthenticatedPlace(loggedUser.username, eatingPlace.id)

        place.apply {
            name = eatingPlace.name
            description = eatingPlace.description
            allergenStrategy = AllergenStrategy.valueOf(eatingPlace.allergenStrategy)
            phoneNumber = eatingPlace.phoneNumber
            website = eatingPlace.website
            email = eatingPlace.email
            address = with(eatingPlace.address) {
                Address(street, number, city, postalCode, latitude, longitude)
            }
            facebookPage = eatingPlace.facebookPage
            twitterPage = eatingPlace.twitterPage
            pysznePlPage = eatingPlace.pysznePlPage
            pizzaPortalPage = eatingPlace.pizzaPortalPage
        }

        eatingPlaceRepository.save(place)
    }

    fun deleteEatingPlace(loggedUser: OAuth2UserDto, idToDelete: String) {
        val place = getAuthenticatedPlace(loggedUser.username, idToDelete)
        place.deleted = true
        eatingPlaceRepository.save(place)
    }

    fun removeUserFromPlace(loggedUser: OAuth2UserDto, placeId: String, userId: String) {
        val place = getAuthenticatedPlace(loggedUser.username, placeId)
        val user = place.users.find { it.id == userId } ?: throw Exception("USER_NOT_FOUND")
        if (!place.users.contains(user)) {
            throw Exception("USER_DOESNT_BELONG_TO_PLACE")
        }
        place.apply { users = users.minus(user) }
        user.apply { eatingPlaces = eatingPlaces.minus(place) }
        eatingPlaceRepository.save(place)
        userRepository.save(user)
    }

    fun joinToPlace(loggedUser: OAuth2UserDto, body: CreateJoinToPlace) {
        val user = userService.getUser(loggedUser.username)
        val place = getPlace(body.placeId)
        if (place.users.contains(user)) {
            throw Exception("ALREADY_JOINED")
        }
        if (place.joinToPlaceCode != body.joinCode) {
            throw Exception("BAD_JOIN_TO_PLACE_CODE")
        }
        user.apply { eatingPlaces = eatingPlaces.plus(place) }
        place.apply { users = users.plus(user) }
        eatingPlaceRepository.save(place)
        userRepository.save(user)
    }

}
