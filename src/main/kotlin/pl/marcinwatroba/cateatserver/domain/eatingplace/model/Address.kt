package pl.marcinwatroba.cateatserver.domain.eatingplace.model

import javax.persistence.Embeddable

/**
 * Created by Marcin
 */
@Embeddable
data class Address(
        val street: String,
        val number: String,
        val city: String,
        val postalCode: String,
        val latitude: Double?,
        val longitude: Double?
)