package pl.marcinwatroba.cateatserver.domain.eatingplace.dto

import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import java.util.*

/**
 * Created by Marcin
 */
data class EatingPlaceDto(
        var id: String = UUID.randomUUID().toString(),
        var name: String,
        var description: String,
        val allergenStrategy: String,
        val address: AddressDto,
        val phoneNumber: String?,
        val email: String?,
        val website: String?,
        var facebookPage: String?,
        var twitterPage: String?,
        var pysznePlPage: String?,
        var pizzaPortalPage: String?
) {

    companion object {

        fun create(place: EatingPlaceEntity) = with(place) {
            EatingPlaceDto(id, name, description, allergenStrategy.name, AddressDto.create(address),
                    phoneNumber, email, website, facebookPage, twitterPage, pysznePlPage, pizzaPortalPage)
        }
    }

}