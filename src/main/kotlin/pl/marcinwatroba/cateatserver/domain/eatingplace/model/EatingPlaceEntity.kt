package pl.marcinwatroba.cateatserver.domain.eatingplace.model

import pl.marcinwatroba.cateatserver.domain.announcement.model.AnnouncementEntity
import pl.marcinwatroba.cateatserver.domain.firebasemessage.model.FirebaseMessageEntity
import pl.marcinwatroba.cateatserver.domain.firebasemessagedevice.model.FirebaseMessageDeviceEntity
import pl.marcinwatroba.cateatserver.domain.mealcategory.model.MealCategoryEntity
import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementEntity
import pl.marcinwatroba.cateatserver.domain.user.UserEntity
import java.util.*
import javax.persistence.*

/**
 * Created by Marcin
 */
@Entity
data class EatingPlaceEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var name: String,
        var description: String,
        @Enumerated(value = EnumType.STRING)
        var allergenStrategy: AllergenStrategy,
        var joinToPlaceCode: String,
        var deleted: Boolean = false,

        @Embedded
        var address: Address,
        var phoneNumber: String?,
        var email: String?,
        var website: String?,
        var facebookPage: String?,
        var twitterPage: String?,
        var pysznePlPage: String?,
        var pizzaPortalPage: String?,

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "eatingPlace")
        var announcements: Set<AnnouncementEntity> = emptySet(),

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "eatingPlace")
        var firebaseMessages: Set<FirebaseMessageEntity> = emptySet(),

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "eatingPlace")
        var mealCategories: Set<MealCategoryEntity> = emptySet(),

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "eatingPlace")
        var menuElements: Set<MenuElementEntity> = emptySet(),

        @ManyToMany(fetch = FetchType.LAZY, mappedBy = "eatingPlaces")
        var users: Set<UserEntity> = emptySet(),

        @ManyToMany(fetch = FetchType.LAZY, mappedBy = "eatingPlace")
        var firebaseMessageDevices: Set<FirebaseMessageDeviceEntity> = emptySet()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EatingPlaceEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (allergenStrategy != other.allergenStrategy) return false
        if (address != other.address) return false
        if (phoneNumber != other.phoneNumber) return false
        if (email != other.email) return false
        if (website != other.website) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + allergenStrategy.hashCode()
        result = 31 * result + address.hashCode()
        result = 31 * result + (phoneNumber?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (website?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "EatingPlaceEntity(id='$id', name='$name', description='$description'," +
                " allergenStrategy=$allergenStrategy, address=$address, phoneNumber=$phoneNumber, email=$email," +
                " website=$website)"
    }

}
