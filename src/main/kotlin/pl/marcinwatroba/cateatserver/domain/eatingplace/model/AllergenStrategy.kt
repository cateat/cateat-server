package pl.marcinwatroba.cateatserver.domain.eatingplace.model

/**
 * Created by Marcin
 */
enum class AllergenStrategy {
    IN_MENU, ASK_LOCALLY
}
