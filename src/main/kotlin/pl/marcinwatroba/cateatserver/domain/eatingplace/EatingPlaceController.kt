package pl.marcinwatroba.cateatserver.domain.eatingplace

import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.eatingplace.dto.CreateJoinToPlace
import pl.marcinwatroba.cateatserver.domain.eatingplace.dto.EatingPlaceDto
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("/place")
class EatingPlaceController(
        private val eatingPlaceService: EatingPlaceService
) {

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun getUserEatingPlaces(@AuthenticationPrincipal loggedUser: OAuth2UserDto) =
            eatingPlaceService.getUserEatingPlaces(loggedUser)

    @GetMapping("/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun getUserPlaceById(@AuthenticationPrincipal loggedUser: OAuth2UserDto,
                         @PathVariable placeId: String) =
            eatingPlaceService.getUserPlaceById(loggedUser, placeId)

    @GetMapping("/{placeId}/joinToPlaceCode")
    @ResponseStatus(HttpStatus.OK)
    fun getJoinToPlaceCode(@AuthenticationPrincipal loggedUser: OAuth2UserDto,
                           @PathVariable placeId: String) =
            eatingPlaceService.getJoinToPlaceCode(loggedUser, placeId)

    @PatchMapping("/{placeId}/joinToPlaceCode")
    @ResponseStatus(HttpStatus.OK)
    fun changeJoinToPlaceCode(@AuthenticationPrincipal loggedUser: OAuth2UserDto,
                              @PathVariable placeId: String) =
            eatingPlaceService.changeJoinToPlaceCode(loggedUser, placeId)

    @DeleteMapping("/{idToDelete}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteEatingPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable idToDelete: String) {
        eatingPlaceService.deleteEatingPlace(loggedUser, idToDelete)
    }

    @PostMapping("")
    fun addEatingPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @RequestBody body: EatingPlaceDto) {
        eatingPlaceService.addEatingPlace(loggedUser, body)
    }

    @PutMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun editEatingPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @RequestBody body: EatingPlaceDto) {
        eatingPlaceService.editEatingPlace(loggedUser, body)
    }

    @DeleteMapping("/{placeId}/deleteUser/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun removeUserFromPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                            @PathVariable userId: String) {
        eatingPlaceService.removeUserFromPlace(loggedUser, placeId, userId)
    }

    @PostMapping("/joinToPlace")
    @ResponseStatus(HttpStatus.OK)
    fun joinToPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @RequestBody body: CreateJoinToPlace) {
        eatingPlaceService.joinToPlace(loggedUser, body)
    }

}