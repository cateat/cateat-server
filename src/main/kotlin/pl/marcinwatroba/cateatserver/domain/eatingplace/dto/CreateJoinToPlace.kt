package pl.marcinwatroba.cateatserver.domain.eatingplace.dto

/**
 * Created by Marcin
 */
data class CreateJoinToPlace(
        val placeId: String,
        val joinCode: String
)