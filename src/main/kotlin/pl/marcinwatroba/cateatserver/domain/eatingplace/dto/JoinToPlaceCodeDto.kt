package pl.marcinwatroba.cateatserver.domain.eatingplace.dto

import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity

/**
 * Created by Marcin
 */
data class JoinToPlaceCodeDto(
        val code: String
) {
    companion object {
        fun create(eatingPlace: EatingPlaceEntity) = JoinToPlaceCodeDto(eatingPlace.joinToPlaceCode)
    }
}