package pl.marcinwatroba.cateatserver.domain.eatingplace.dto

import pl.marcinwatroba.cateatserver.domain.eatingplace.model.Address
import java.util.*

/**
 * Created by Marcin
 */
data class AddressDto(
        var street: String,
        var number: String,
        var city: String,
        var postalCode: String,
        var latitude: Double?,
        var longitude: Double?
) {

    companion object {

        fun create(address: Address) = with(address) {
            AddressDto(street, number, city, postalCode, latitude, longitude)
        }
    }

}