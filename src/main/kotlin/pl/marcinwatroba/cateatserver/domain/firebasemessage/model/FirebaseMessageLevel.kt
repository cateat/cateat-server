package pl.marcinwatroba.cateatserver.domain.firebasemessage.model

/**
 * Created by Marcin
 */
enum class FirebaseMessageLevel {
    NOTHING,
    ONLY_SPECIAL,
    ALL
}