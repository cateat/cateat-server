package pl.marcinwatroba.cateatserver.domain.firebasemessage

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceRepository
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceService
import pl.marcinwatroba.cateatserver.domain.firebasemessage.model.*
import pl.marcinwatroba.cateatserver.domain.user.UserRepository
import pl.marcinwatroba.cateatserver.domain.user.UserService
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@Service
class FirebaseMessageService(
        private val userService: UserService,
        private val eatingPlaceService: EatingPlaceService,
        private val firebaseMessageRepository: FirebaseMessageRepository,
        private val userRepository: UserRepository,
        private val eatingPlaceRepository: EatingPlaceRepository
) {

    fun getFirebaseMessages(loggedUser: OAuth2UserDto, placeId: String): Set<FirebaseMessageDto> {
        userService.getUser(loggedUser.username)
        val place = eatingPlaceService.getAuthenticatedPlace(loggedUser.username, placeId)
        return place.firebaseMessages.map { FirebaseMessageDto.create(it) }.toSet()
    }

    fun sendFirebaseMessage(loggedUser: OAuth2UserDto, placeId: String, body: CreateFirebaseMessage) {
        val user = userService.getUser(loggedUser.username)
        val place = eatingPlaceService.getAuthenticatedPlace(loggedUser.username, placeId)
        val message = FirebaseMessageEntity(title = body.title, content = body.content,
                level = FirebaseMessageLevel.valueOf(body.level), user = user, eatingPlace = place)
        user.apply { firebaseMessages = firebaseMessages.plus(message) }
        place.apply { firebaseMessages = firebaseMessages.plus(message) }
        firebaseMessageRepository.save(message)
        eatingPlaceRepository.save(place)
        userRepository.save(user)
        // send messages - async if possible
    }

}