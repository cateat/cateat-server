package pl.marcinwatroba.cateatserver.domain.firebasemessage

import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.firebasemessage.model.CreateFirebaseMessage
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("/firebaseMessage")
class FirebaseMessageController(
        private val firebaseMessageService: FirebaseMessageService
) {

    @GetMapping("/{placeId}")
    fun getFirebaseMessages(@AuthenticationPrincipal loggedUser: OAuth2UserDto,
                            @PathVariable placeId: String) =
            firebaseMessageService.getFirebaseMessages(loggedUser, placeId)

    @PostMapping("/{placeId}")
    fun sendFirebaseMessage(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                            @RequestBody body: CreateFirebaseMessage) {
        firebaseMessageService.sendFirebaseMessage(loggedUser, placeId, body)
    }

}