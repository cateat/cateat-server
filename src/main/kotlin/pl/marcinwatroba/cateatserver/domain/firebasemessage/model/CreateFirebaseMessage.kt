package pl.marcinwatroba.cateatserver.domain.firebasemessage.model

/**
 * Created by Marcin
 */
data class CreateFirebaseMessage(
        var title: String,
        var content: String,
        var level: String
)