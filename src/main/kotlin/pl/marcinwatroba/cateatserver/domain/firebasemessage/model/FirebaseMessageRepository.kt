package pl.marcinwatroba.cateatserver.domain.firebasemessage.model

import org.springframework.data.repository.CrudRepository

/**
 * Created by Marcin
 */
interface FirebaseMessageRepository : CrudRepository<FirebaseMessageEntity, String>