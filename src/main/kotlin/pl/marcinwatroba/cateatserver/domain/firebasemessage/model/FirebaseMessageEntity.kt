package pl.marcinwatroba.cateatserver.domain.firebasemessage.model

import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import pl.marcinwatroba.cateatserver.domain.user.UserEntity
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

/**
 * Created by Marcin
 */
@Entity
data class FirebaseMessageEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var title: String,
        var content: String,
        var level: FirebaseMessageLevel,
        var created: LocalDateTime = LocalDateTime.now(),

        @ManyToOne
        var user: UserEntity,

        @ManyToOne
        var eatingPlace: EatingPlaceEntity
) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as FirebaseMessageEntity

                if (id != other.id) return false
                if (title != other.title) return false
                if (content != other.content) return false
                if (level != other.level) return false
                if (created != other.created) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id.hashCode()
                result = 31 * result + title.hashCode()
                result = 31 * result + content.hashCode()
                result = 31 * result + level.hashCode()
                result = 31 * result + created.hashCode()
                return result
        }

        override fun toString(): String {
                return "FirebaseMessageEntity(id='$id', title='$title', content='$content', level=$level, " +
                        "created=$created)"
        }
}