package pl.marcinwatroba.cateatserver.domain.firebasemessage.model

import java.time.LocalDateTime

/**
 * Created by Message
 */
data class FirebaseMessageDto(
        val id: String,
        val title: String,
        val content: String,
        val level: String,
        val created: LocalDateTime
) {
    companion object {
        fun create(firebaseMessageEntity: FirebaseMessageEntity) = with(firebaseMessageEntity) {
            FirebaseMessageDto(id, title, content, level.name, created)
        }
    }
}