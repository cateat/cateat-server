package pl.marcinwatroba.cateatserver.domain.announcement

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.announcement.model.AnnouncementDto
import pl.marcinwatroba.cateatserver.domain.announcement.model.AnnouncementEntity
import pl.marcinwatroba.cateatserver.domain.announcement.model.AnnouncementRepository
import pl.marcinwatroba.cateatserver.domain.announcement.model.CreateAnnouncement
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceRepository
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceService
import pl.marcinwatroba.cateatserver.domain.user.UserRepository
import pl.marcinwatroba.cateatserver.global.extension.getOrThrow
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@Service
class AnnouncementService(
        private val userRepository: UserRepository,
        private val announcementRepository: AnnouncementRepository,
        private val eatingPlaceRepository: EatingPlaceRepository,
        private val eatingPlaceService: EatingPlaceService
) {

    private fun getUser(email: String) = userRepository.findUserByEmail(email)
            ?: throw Exception("USER_NOT_FOUND")

    private fun getAuthenticatedPlace(email: String, placeId: String) = getUser(email).eatingPlaces
            .find { it.id == placeId } ?: throw Exception("PLACE_NOT_FOUND")

    private fun getAuthenticatedAnnouncement(email: String, placeId: String,
                                             announcementId: String): AnnouncementEntity {
        getAuthenticatedPlace(email, placeId)
        val announcement = announcementRepository.findById(announcementId)
                .getOrThrow("ANNOUNCEMENT_NOT_FOUND")
        if (announcement.eatingPlace.id != placeId) {
            throw Exception("ANNOUNCEMENT_NOT_FOUND")
        }
        return announcement
    }

    fun getAllForPlace(loggedUser: OAuth2UserDto, placeId: String): Set<AnnouncementDto> {
        val place = eatingPlaceService.getAuthenticatedPlace(loggedUser.username, placeId);
        return announcementRepository.getAllByDeletedIsFalseAndEatingPlace(place)
                .map { AnnouncementDto.create(it) }.toSet()
    }

    fun getActiveForPlace(placeId: String): Set<AnnouncementDto> {
        val place = eatingPlaceService.getPlace(placeId)
        return announcementRepository.getAllByDeletedIsFalseAndEatingPlaceAndActiveIsTrue(place)
                .map { AnnouncementDto.create(it) }.toSet()
    }

    fun addAnnouncement(loggedUser: OAuth2UserDto, placeId: String, body: CreateAnnouncement) {
        val place = getAuthenticatedPlace(loggedUser.username, placeId)
        val newAnnouncement = AnnouncementEntity(name = body.name, description = body.description,
                active = body.isActive, eatingPlace = place)
        place.announcements = place.announcements.plus(newAnnouncement)
        announcementRepository.save(newAnnouncement)
        eatingPlaceRepository.save(place)
    }

    fun editAnnouncement(loggedUser: OAuth2UserDto, placeId: String, body: CreateAnnouncement) {
        val announcement = getAuthenticatedAnnouncement(loggedUser.username, placeId, body.id!!)
        announcement.apply {
            name = body.name
            description = body.description
            active = body.isActive
        }
        announcementRepository.save(announcement)
    }

    fun deleteAnnouncement(loggedUser: OAuth2UserDto, placeId: String, announcementId: String) {
        val announcement = getAuthenticatedAnnouncement(loggedUser.username, placeId, announcementId)
        announcement.deleted = true
        announcementRepository.save(announcement)
    }

}