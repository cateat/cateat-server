package pl.marcinwatroba.cateatserver.domain.announcement.model

import org.springframework.data.annotation.LastModifiedDate
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

/**
 * Created by Marcin
 */
@Entity
data class AnnouncementEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var name: String,
        var description: String,
        var created: LocalDateTime = LocalDateTime.now(),
        var active: Boolean = true,
        var deleted: Boolean = false,

        @LastModifiedDate
        var lastUpdate: LocalDateTime? = null,

        @ManyToOne
        var eatingPlace: EatingPlaceEntity
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AnnouncementEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (created != other.created) return false
        if (active != other.active) return false
        if (deleted != other.deleted) return false
        if (lastUpdate != other.lastUpdate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + created.hashCode()
        result = 31 * result + active.hashCode()
        result = 31 * result + deleted.hashCode()
        result = 31 * result + (lastUpdate?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "AnnouncementEntity(id='$id', name='$name', description='$description', created=$created, " +
                "active=$active, deleted=$deleted, lastUpdate=$lastUpdate)"
    }

}