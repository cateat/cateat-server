package pl.marcinwatroba.cateatserver.domain.announcement.model

import org.springframework.data.repository.CrudRepository
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity

/**
 * Created by Marcin
 */
interface AnnouncementRepository : CrudRepository<AnnouncementEntity, String> {
    fun getAllByDeletedIsFalseAndEatingPlace(place: EatingPlaceEntity): List<AnnouncementEntity>

    fun getAllByDeletedIsFalseAndEatingPlaceAndActiveIsTrue(place: EatingPlaceEntity): List<AnnouncementEntity>
}