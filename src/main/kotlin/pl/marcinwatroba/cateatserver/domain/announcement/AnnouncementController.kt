package pl.marcinwatroba.cateatserver.domain.announcement

import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.announcement.model.CreateAnnouncement
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("/announcement")
class AnnouncementController(
        private val announcementService: AnnouncementService
) {

    @PostMapping("/{placeId}")
    @ResponseStatus(HttpStatus.CREATED)
    fun addAnnouncement(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                        @RequestBody body: CreateAnnouncement) {
        announcementService.addAnnouncement(loggedUser, placeId, body)
    }

    @PutMapping("/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun editAnnouncement(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                         @RequestBody body: CreateAnnouncement) {
        announcementService.editAnnouncement(loggedUser, placeId, body)
    }

    @GetMapping("/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun getAllForPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto,
                       @PathVariable placeId: String) =
            announcementService.getAllForPlace(loggedUser, placeId)

    @GetMapping("/active/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun getActiveForPlace(@PathVariable placeId: String) =
            announcementService.getActiveForPlace(placeId)

    @DeleteMapping("/{placeId}/{announcementId}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteAnnouncement(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                           @PathVariable announcementId: String) =
            announcementService.deleteAnnouncement(loggedUser, placeId, announcementId)

}