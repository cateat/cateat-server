package pl.marcinwatroba.cateatserver.domain.announcement.model

import java.time.LocalDateTime
import java.util.*

/**
 * Created by Marcin
 */
data class AnnouncementDto(
        val id: String = UUID.randomUUID().toString(),
        val name: String,
        val description: String,
        val created: LocalDateTime,
        val isActive: Boolean,
        val lastUpdate: LocalDateTime?
) {
    companion object {
        fun create(announcementEntity: AnnouncementEntity) = with(announcementEntity) {
            AnnouncementDto(id, name, description, created, active, lastUpdate)
        }
    }
}