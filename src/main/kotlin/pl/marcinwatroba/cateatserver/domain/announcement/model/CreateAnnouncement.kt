package pl.marcinwatroba.cateatserver.domain.announcement.model

/**
 * Created by Marcin
 */
data class CreateAnnouncement(
        val id: String?,
        val name: String,
        val description: String,
        val isActive: Boolean
)