package pl.marcinwatroba.cateatserver.domain.menuelement.model

import pl.marcinwatroba.cateatserver.domain.allergen.Allergen
import pl.marcinwatroba.cateatserver.domain.eatingplace.model.EatingPlaceEntity
import java.util.*
import javax.persistence.*

/**
 * Created by Marcin
 */
@Entity
data class MenuElementEntity(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var name: String,
        var description: String?,
        var price: Double?,
        var deleted: Boolean,

        @ManyToOne
        var eatingPlace: EatingPlaceEntity,

        @ManyToMany(fetch = FetchType.LAZY,
                cascade = [
                    CascadeType.PERSIST,
                    CascadeType.MERGE
                ])
        @JoinTable(name = "allergenInMenuElement",
                joinColumns = [JoinColumn(name = "menuElementId")],
                inverseJoinColumns = [JoinColumn(name = "AllergenId")])
        var allergens: Set<Allergen>

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MenuElementEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (price != other.price) return false
        if (deleted != other.deleted) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (price?.hashCode() ?: 0)
        result = 31 * result + deleted.hashCode()
        return result
    }

    override fun toString(): String {
        return "MenuElementEntity(id='$id', name='$name', description=$description, price=$price, deleted=$deleted)"
    }
}