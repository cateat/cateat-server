package pl.marcinwatroba.cateatserver.domain.menuelement

import org.springframework.stereotype.Service
import pl.marcinwatroba.cateatserver.domain.allergen.AllergenRepository
import pl.marcinwatroba.cateatserver.domain.allergen.AllergenService
import pl.marcinwatroba.cateatserver.domain.eatingplace.EatingPlaceService
import pl.marcinwatroba.cateatserver.domain.menuelement.model.CreateMenuElement
import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementDto
import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementEntity
import pl.marcinwatroba.cateatserver.domain.menuelement.model.MenuElementRepository
import pl.marcinwatroba.cateatserver.global.extension.getOrThrow
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@Service
class MenuElementService(
        private val menuElementRepository: MenuElementRepository,
        private val allergenService: AllergenService,
        private val allergenRepository: AllergenRepository,
        private val eatingPlaceService: EatingPlaceService
) {

    fun getAuthenticatedElement(userEmail: String, placeId: String, elementId: String): MenuElementEntity {
        eatingPlaceService.getAuthenticatedPlace(userEmail, placeId)
        val element = menuElementRepository.findById(elementId)
                .getOrThrow("ELEMENT_NOT_FOUND")
        if (element.eatingPlace.id != placeId) {
            throw Exception("ELEMENT_NOT_FOUND")
        }
        return element
    }

    fun addMenuElement(loggedUser: OAuth2UserDto, placeId: String, body: CreateMenuElement) {
        val place = eatingPlaceService.getAuthenticatedPlace(loggedUser.username, placeId)
        val allergens = allergenService.convertSetToEntities(body.allergens)

        val newMealElement = MenuElementEntity(name = body.name, description = body.description, price = body.price,
                deleted = false, eatingPlace = place, allergens = allergens)

        allergens.forEach {
            it.menuElements = it.menuElements.plus(newMealElement)
        }
        menuElementRepository.save(newMealElement)
        allergenRepository.saveAll(allergens)
    }

    fun deleteMenuElement(loggedUser: OAuth2UserDto, placeId: String, elementId: String) {
        val element = getAuthenticatedElement(loggedUser.username, placeId, elementId)
        if (element.deleted) {
            throw Exception("ELEMENT_IS_ALREADY_DELETED")
        }
        element.deleted = true
        menuElementRepository.save(element)
    }

    fun activateElement(loggedUser: OAuth2UserDto, placeId: String, elementId: String) {
        val element = getAuthenticatedElement(loggedUser.username, placeId, elementId)
        if (element.deleted) {
            throw Exception("ELEMENT_IS_ALREADY_DELETED")
        }
        element.deleted = true
        menuElementRepository.save(element)
    }

    fun editMenuElement(loggedUser: OAuth2UserDto, placeId: String, body: CreateMenuElement) {
        val element = getAuthenticatedElement(loggedUser.username, placeId, body.id!!)
        element.apply {
            name = body.name
            description = body.description
            price = body.price
        }
        val allergens = allergenService.convertSetToEntities(body.allergens)
        val toRemove = element.allergens.minus(allergens)
        toRemove.forEach {
            element.allergens = element.allergens.minus(it)
            it.menuElements = it.menuElements.minus(element)
        }
        allergenRepository.saveAll(toRemove)

        val toAdd = allergens.minus(element.allergens)

        toAdd.forEach {
            it.menuElements = it.menuElements.plus(element)
        }
        element.allergens = element.allergens.plus(toAdd)
        menuElementRepository.save(element)
        allergenRepository.saveAll(toAdd)
    }

    fun getAllMenuElementsForPlace(loggedUser: OAuth2UserDto, placeId: String): Set<MenuElementDto> {
        val place = eatingPlaceService.getAuthenticatedPlace(loggedUser.username, placeId)
        return place.menuElements.map { MenuElementDto.create(it) }.toSet()
    }

}