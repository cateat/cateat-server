package pl.marcinwatroba.cateatserver.domain.menuelement

import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import pl.marcinwatroba.cateatserver.domain.menuelement.model.CreateMenuElement
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto

/**
 * Created by Marcin
 */
@RestController
@RequestMapping("/menuElement")
class MenuElementController(
        private val menuElementService: MenuElementService
) {

    @PostMapping("/{placeId}")
    @ResponseStatus(HttpStatus.CREATED)
    fun addMenuElement(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                       @RequestBody body: CreateMenuElement) {
        menuElementService.addMenuElement(loggedUser, placeId, body)
    }

    @DeleteMapping("/{placeId}/{idToDelete}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteMenuElement(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                          @PathVariable idToDelete: String) {
        menuElementService.deleteMenuElement(loggedUser, placeId, idToDelete)
    }

    @PatchMapping("/activate/{placeId}/{idToDelete}")
    @ResponseStatus(HttpStatus.OK)
    fun activateMenuElement(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                            @PathVariable idToDelete: String) {
        menuElementService.activateElement(loggedUser, placeId, idToDelete)
    }

    @PutMapping("/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun editMenuElement(@AuthenticationPrincipal loggedUser: OAuth2UserDto, @PathVariable placeId: String,
                        @RequestBody body: CreateMenuElement) {
        menuElementService.editMenuElement(loggedUser, placeId, body)
    }

    @GetMapping("/all/{placeId}")
    @ResponseStatus(HttpStatus.OK)
    fun getAllMenuElementsForPlace(@AuthenticationPrincipal loggedUser: OAuth2UserDto,
                                   @PathVariable placeId: String) =
            menuElementService.getAllMenuElementsForPlace(loggedUser, placeId)

}