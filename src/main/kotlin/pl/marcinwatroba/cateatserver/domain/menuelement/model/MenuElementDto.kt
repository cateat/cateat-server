package pl.marcinwatroba.cateatserver.domain.menuelement.model

/**
 * Created by Marcin
 */
data class MenuElementDto(
        val id: String,
        val name: String,
        val description: String?,
        val price: Double?,
        val deleted: Boolean,
        val allergens: Set<String>
) {
    companion object {
        fun create(element: MenuElementEntity) = with(element) {
            MenuElementDto(id, name, description, price, deleted, allergens.map { it.name }.toSet())
        }
    }
}