package pl.marcinwatroba.cateatserver.domain.menuelement.model

/**
 * Created by Marcin
 */
data class CreateMenuElement(
        val id: String?,
        val name: String,
        val description: String?,
        val price: Double?,
        val allergens: Set<String>
)