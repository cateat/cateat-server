package pl.marcinwatroba.cateatserver.domain.menuelement.model

import org.springframework.data.repository.CrudRepository

/**
 * Created by Marcin
 */
interface MenuElementRepository : CrudRepository<MenuElementEntity, String>