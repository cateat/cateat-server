package pl.marcinwatroba.cateatserver.notification.email

import org.springframework.scheduling.annotation.Async
import java.io.File

/**
 * Created by Marcin
 */
interface EmailService {

    @Async
    fun sendSimpleMessage(to: String, subject: String, text: String)

    @Async
    fun sendAttachmentMessage(to: String, subject: String, text: String,
                              attachments: List<Pair<String, File>>)
}