package pl.marcinwatroba.cateatserver.notification.email

import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Component
import java.io.File


/**
 * Created by Marcin
 */
@Component
class EmailServiceImpl(var emailSender: JavaMailSender) : EmailService {

    override fun sendSimpleMessage(to: String, subject: String, text: String) {

        val message = SimpleMailMessage()
        message.setTo(to)
        message.setSubject("$PRE_TITLE $subject")
        message.setText(text)
        emailSender.send(message)
    }

    override fun sendAttachmentMessage(to: String, subject: String, text: String,
                                       attachments: List<Pair<String, File>>) {

        val message = emailSender.createMimeMessage()
        val helper = MimeMessageHelper(message, true)

        helper.setTo(to)
        helper.setSubject("$PRE_TITLE $subject")
        helper.setText(text)
        attachments.forEach { helper.addAttachment(it.first, it.second) }

        emailSender.send(message)
    }

    companion object {
        const val PRE_TITLE = "[CatEat]"
    }

}