package pl.marcinwatroba.cateatserver.global.util

import java.util.*
import kotlin.math.absoluteValue

/**
 * Created by Marcin
 */
object CodeGenerator {

    fun getCode(blocksCount: Int, blockSize: Int) =
            (0 until blocksCount).joinToString("-") { getCodeBlock(blockSize) }

    private fun getCodeBlock(blockSize: Int) =
            (0 until blockSize).joinToString("") { getCharToCode().toString() }

    private fun getCharToCode() = (Random().nextInt().absoluteValue % 26 + 65).toChar()
}