package pl.marcinwatroba.cateatserver.global.extension

import java.util.*

/**
 * Created by Marcin
 */

fun <T> Optional<T>.getOrThrow(errorMessage: String) = getOrNull() ?: throw Exception(errorMessage)

fun <T> Optional<T>.getOrNull() = if (isPresent) get() else null