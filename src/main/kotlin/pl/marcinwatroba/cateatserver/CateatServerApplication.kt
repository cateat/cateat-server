package pl.marcinwatroba.cateatserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication
@EnableAsync
class CateatServerApplication

fun main(args: Array<String>) {
    runApplication<CateatServerApplication>(*args)
}

