package pl.marcinwatroba.cateatserver.security.dto

data class CreateOAuth2UserDto(
        val email: String,
        val password: String
)