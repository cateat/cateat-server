package pl.marcinwatroba.cateatserver.security.dto

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class OAuth2UserDto(
        private val id: String,
        private val email: String,
        private val password: String,
        private val deleted: Boolean = false,
        private val verifiedCode: String?
) : UserDetails {

    override fun getUsername() = email

    override fun getPassword() = password

    override fun getAuthorities(): Collection<GrantedAuthority> = emptyList()

    override fun isAccountNonExpired() = !deleted

    override fun isAccountNonLocked() = !deleted

    override fun isCredentialsNonExpired() = verifiedCode == null

    override fun isEnabled() = !deleted

}
