package pl.marcinwatroba.cateatserver.security.domain

import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Primary
@Service
class OAuth2UserFinder(private val oAuth2UserRepository: OAuth2UserRepository) : UserDetailsService {

    override fun loadUserByUsername(username: String) =
            oAuth2UserRepository.findByEmail(username)?.toDto()

}