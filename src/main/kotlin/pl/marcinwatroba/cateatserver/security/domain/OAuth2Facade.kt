package pl.marcinwatroba.cateatserver.security.domain

import pl.marcinwatroba.cateatserver.security.dto.CreateOAuth2UserDto
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto
import java.util.*
import kotlin.math.absoluteValue

class OAuth2Facade(
        private val oAuth2UserFinder: OAuth2UserFinder,
        private val oAuth2UserCreator: OAuth2UserCreator,
        private val oAuth2UserRepository: OAuth2UserRepository,
        private val oAuth2UserPasswordManager: OAuth2UserPasswordManager
) {

    fun createUserCredentials(createOAuth2UserDto: CreateOAuth2UserDto): OAuth2UserDto {
        return oAuth2UserCreator.createOAuth2UserClaims(createOAuth2UserDto.email, createOAuth2UserDto.password).toDto()
    }

    fun findUserByEmail(username: String) = oAuth2UserFinder.loadUserByUsername(username)
//
//    fun changeEmail(emailFrom: String, emailTo: String) {
//        val user = oAuth2UserRepository.findByEmail(emailFrom)
//                ?: throw IllegalArgumentException("User credentials for email $emailFrom not found!") as Throwable
//
//        val potentialUserWithNewEmail = oAuth2UserRepository.findByEmail(emailTo)
//        if (potentialUserWithNewEmail != null)
//            throw IllegalArgumentException("User credentials for email $emailTo found!")
//
//        user.email = emailTo
//        user.verifyCode = UUID.randomUUID().toString()
//    }

    fun getUserActivationKeys(email: String): Pair<String, String> {
        val account = oAuth2UserRepository.findByEmail(email) ?: throw Exception()
        return Pair(account.id, account.verifyCode ?: throw Exception())
    }

//    fun deleteUserCredentials(username: String) {
//        val oAuth2User = (oAuth2UserRepository.findByEmail(username)
//                ?: throw IllegalArgumentException("User credentials for email $username not found!"))
//        oAuth2UserRepository.delete(oAuth2User)
//    }

//    fun changePassword(email: String, oldPassword: String, newPassword: String) {
//        if (oAuth2UserPasswordManager.passwordMarch(email, oldPassword)) {
//            oAuth2UserPasswordManager.changePassword(email, newPassword)
//        } else {
//            throw Exception("Password don't match")
//        }
//    }

    fun activateAccount(id: String, code: String): ActivationStatus {
        val account = oAuth2UserRepository.findById(id)
        return if (account.isPresent) {
            val user = account.get()
            when {
                user.verifyCode == null -> ActivationStatus.JUST_ACTIVATED
                user.verifyCode == code -> {
                    user.verifyCode = null
                    oAuth2UserRepository.save(user)
                    ActivationStatus.OK
                }
                else -> ActivationStatus.BAD_CREDENTIALS
            }
        } else ActivationStatus.BAD_CREDENTIALS
    }

//    fun resetPassword(email: String): String {
//
//        val user = oAuth2UserRepository.findByEmail(email)
//                ?: throw IllegalArgumentException("User credentials for email $email not found!")
//        val newPassword = generatePassword(8)
//        oAuth2UserPasswordManager.changePassword(email, newPassword)
//
//        return newPassword
//    }

//    private fun generatePassword(size: Int) =
//            (0 until size).map { (Random().nextInt().absoluteValue % 10).toString() }.joinToString("") { it }

    enum class ActivationStatus {
        OK, JUST_ACTIVATED, BAD_CREDENTIALS
    }

}