package pl.marcinwatroba.cateatserver.security.domain

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class OAuth2Configuration {

    @Bean
    fun oAuth2Facade(oAuth2UserRepository: OAuth2UserRepository, passwordEncoder: PasswordEncoder): OAuth2Facade {
        val oAuth2UserCreator = OAuth2UserCreator(oAuth2UserRepository, passwordEncoder)
        val oAuth2UserFinder = OAuth2UserFinder(oAuth2UserRepository)
        val oAuth2UserPasswordManager = OAuth2UserPasswordManager(oAuth2UserRepository, passwordEncoder)
        return OAuth2Facade(oAuth2UserFinder, oAuth2UserCreator, oAuth2UserRepository, oAuth2UserPasswordManager)
    }
}