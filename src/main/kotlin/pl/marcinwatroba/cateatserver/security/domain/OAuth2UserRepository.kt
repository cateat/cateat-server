package pl.marcinwatroba.cateatserver.security.domain

import org.springframework.data.jpa.repository.JpaRepository
import pl.marcinwatroba.cateatserver.security.domain.OAuth2User

interface OAuth2UserRepository: JpaRepository<OAuth2User, String>{

    fun findByEmail(email: String) : OAuth2User?

}