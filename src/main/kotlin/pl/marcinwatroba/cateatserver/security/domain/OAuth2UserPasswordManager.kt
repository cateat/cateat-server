package pl.marcinwatroba.cateatserver.security.domain

import org.springframework.security.crypto.password.PasswordEncoder

/**
 * Created by Marcin
 */

class OAuth2UserPasswordManager(
        private val oAuth2UserRepository: OAuth2UserRepository,
        private val passwordEncoder: PasswordEncoder
) {
    fun changePassword(email: String, newPassword: String) {
        val user = oAuth2UserRepository.findByEmail(email)
                ?: throw IllegalArgumentException("Email of account to change password $email not found")
        user.password = passwordEncoder.encode(newPassword)
        oAuth2UserRepository.save(user)
    }

    fun passwordMarch(email: String, password: String): Boolean {
        val user = oAuth2UserRepository.findByEmail(email)
                ?: throw IllegalArgumentException("Email of account to change password $email not found")
        return passwordEncoder.matches(password, user.password)
    }
}