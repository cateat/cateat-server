package pl.marcinwatroba.cateatserver.security.domain

import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class OAuth2User(
        @Id
        var id: String = UUID.randomUUID().toString(),
        var email: String = "",
        var password: String = "",
        var isDeleted: Boolean = false,
        var verifyCode: String? = UUID.randomUUID().toString(),
        var resetPasswordCode: String? = null
) {

    fun toDto() = OAuth2UserDto(id, email, password, isDeleted, verifyCode)

    override fun hashCode() = id.hashCode()

    override fun equals(other: Any?): Boolean {
        return other != null && other is OAuth2User && email == other.email && id == other.id
                && verifyCode == other.verifyCode
    }

}
