package pl.marcinwatroba.cateatserver.security.domain

import org.springframework.security.crypto.password.PasswordEncoder
import pl.marcinwatroba.cateatserver.security.dto.OAuth2UserDto


class OAuth2UserCreator(
        private val oAuth2UserRepository: OAuth2UserRepository,
        private val passwordEncoder: PasswordEncoder
) {

    fun createOAuth2UserClaims(username: String, password: String)
            : OAuth2User {
        val user = OAuth2User(email = username, password = passwordEncoder.encode(password))
        oAuth2UserRepository.save(user)
        return user
    }

}
